﻿namespace DirectoryAnalyzer
{
    public class DirectoryAnalyzer
    {
        private readonly string _directoryPath;

        private bool _analyzingStopped = false;

        public event EventHandler<FileArgs>? FileFound;

        public DirectoryAnalyzer(string directoryPath)
        {
            if (string.IsNullOrEmpty(directoryPath))
            {
                throw new ArgumentNullException(nameof(directoryPath));
            }
            if (!Directory.Exists(directoryPath))
            {
                throw new DirectoryNotFoundException(nameof(directoryPath));
            }
            _directoryPath = directoryPath;
        }

        public void Start()
        {
            _analyzingStopped = false;
            var fileNames = Directory.EnumerateFiles(_directoryPath);

            foreach (var fileName in fileNames)
            {
                if (_analyzingStopped)
                {
                    return;
                }
                var eventArgs = new FileArgs(fileName);
                FileFound?.Invoke(this, eventArgs);
                Thread.Sleep(1000);
            }
        }

        public void Stop()
        {
            _analyzingStopped = true;
        }
    }
}
