﻿namespace DirectoryAnalyzer
{
    public class FileArgs : EventArgs
    {
        public FileArgs(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                throw new ArgumentNullException(nameof(fileName));
            }

            FileName = fileName;
        }
        public string FileName { get; private set; }
    }
}