﻿using DirectoryAnalyzer;

static void CommonFileFoundHandler(object sender, FileArgs e)
{
    Console.WriteLine($"Найден другой файл - {e.FileName}");
}

static void InterruptingFileFoundHandler(object sender, FileArgs e)
{
    Console.WriteLine($"Найден файл - {e.FileName}");
    ((DirectoryAnalyzer.DirectoryAnalyzer)sender).Stop();
}

Console.Clear();

var directoryAnalyzer = new DirectoryAnalyzer.DirectoryAnalyzer(AppDomain.CurrentDomain.BaseDirectory);

Console.WriteLine("Запуск анализа файлов в текущем каталоге (common listener attached)");
Console.WriteLine();
directoryAnalyzer.FileFound += CommonFileFoundHandler;
directoryAnalyzer.Start();

Console.WriteLine();
Console.WriteLine("Запуск анализа файлов в текущем каталоге (common listener detached)");
directoryAnalyzer.FileFound -= CommonFileFoundHandler;
directoryAnalyzer.Start();

Console.WriteLine();
Console.WriteLine("Запуск анализа файлов в текущем каталоге (interrupting listener attached)");
Console.WriteLine();
directoryAnalyzer.FileFound += InterruptingFileFoundHandler;
directoryAnalyzer.Start();

Console.WriteLine();
Console.Write("Нажмите любую кнопку...");
Console.ReadKey();