﻿namespace MaxElementSearcher
{
    public static class IEnumerableExtensions
    {
        public static T GetMax<T>(
            this IEnumerable<T> collection,
            Func<T, float> convertor) where T : class
        {
            if (collection == null || !collection.Any())
            {
                throw new ArgumentException("Коллекция пустая");
            }
            var max = collection.First();
            for (var i = 1; i < collection.Count(); i++)
            {
                var current = collection.ElementAt(i);
                if (convertor(current) > convertor(max))
                {
                    max = current;
                }
            }
            return max;
        }
    }
}