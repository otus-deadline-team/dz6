﻿using MaxElementSearcher;

static float GenericToFloat<T>(T element) where T : class
{
    return element.GetHashCode();
}

Console.Clear();

var users = new[]
{
    new User { Id = Guid.NewGuid(), Name = "Владимир Владимирович"},
    new User { Id = Guid.NewGuid(), Name = "Ефремова Анна"},
    new User { Id = Guid.NewGuid(), Name = "Козловский Даниил"},
    new User { Id = Guid.NewGuid(), Name = "Афанасьев Алексей" }
};

Console.WriteLine("Пользователи: ");
foreach (var user in users)
{
    Console.WriteLine(user);
}

Console.WriteLine();
Console.WriteLine($"Максимальный элемент коллекции: {users.GetMax(GenericToFloat)}");

Console.WriteLine();
Console.Write("Нажмите любую кнопку...");
Console.ReadKey();